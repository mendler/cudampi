#ifndef _GENERATOR_H_
#define _GENERATOR_H_

int generate(unsigned long long num_vals, char *filename, int rank);

#endif
