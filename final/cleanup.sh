#!/bin/bash

if [[ $1 == "all" ]]
then
	rm /tmp-neu/sorted/*
	rm /tmp-neu/pending/*
	rm /tmp-neu/buckets/*
elif [[ $1 == "restart" ]]
then
	mv /tmp-neu/pending/* /tmp-neu/buckets/.
fi
