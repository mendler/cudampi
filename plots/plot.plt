# Plotten von gemessenen Daten
#=============================
set title 'MPI-IO write performance'
set xrange [0:67108865]
set yrange [0:10]
set xlabel 'Blockgroesse [Byte]'
set ylabel 'Zeit [Sekunden]'
set terminal png
set output "performanz.png"
plot 'data.dat' using 1:2 with lines
replot

