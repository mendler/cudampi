#include <stdio.h>

main() {

  int up;
  int size = 8; /* Number of processors */
  int amount = 4; /* Amount of values for each processor. */
  int i, j;

  for (up=8;up<=amount*size;up=2*up)
  {
      printf("Major step: up=%d\n", up);
      for (j=up>>1;j>0;j=j>>1)
      {
          printf("Minor step: j=%d\n", j);
          for (i=0;i<amount*size;i++)
          {
              int partner=i^j;
              if ((partner)>i) {
                  printf("exchange(%d,%d,%d)\n", i, partner, i&up);
              }
          }
      }
  }
}
