/**
 * Uebungsblatt 1 - VPS II
 * Munder, Malte 1129854
 * Endler, Matthias 1130144
 */

#include <mpi.h>
#include <unistd.h>
#include <stdio.h>
#include <time.h>

int aufgabe1(int argc, char** argv);
int aufgabe2(int argc, char** argv);
int aufgabe3(int argc, char** argv);

int main(int argc, char** argv) {
  //aufgabe1(argc, argv);
  //aufgabe2(argc, argv);
  aufgabe3(argc, argv);
}

int aufgabe1(int argc, char** argv){
	int buf = 13;
	int root = 0;
	int receiver = 1;
	int inbuf;
	MPI_Status status;

	struct timespec start, end;
	start.tv_sec = 5;
	start.tv_nsec = 0;

	if(MPI_Init(&argc, &argv) != MPI_SUCCESS){
		printf("init failed!\n");
		//bricht ab
		return -1;
	}
	int my_rank, size;
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	if(my_rank == root){
		MPI_Send(&buf, 1, MPI_INT, receiver, 0, MPI_COMM_WORLD);
		printf("root: sent");
		buf = 42; // Inhalt des Puffers ändern.
	}
	else if (my_rank == receiver) {
		printf("receiver: sleeping...\n");
		nanosleep(&start, &end);
		MPI_Recv(&inbuf, 1, MPI_INT, root, 0, MPI_COMM_WORLD, &status);
		printf("receiver: received %d\n", inbuf);
	}
	MPI_Finalize();
	return 0;
}

////////////////////////////////////////////////////////////////////////////


/*
int fill_buf(int* buf, int bufsize) {
  int i;
  for (i = 0; i < bufsize; ++i) {
    buf[i] = 0;
  }
}
*/

int aufgabe2(int argc, char** argv) {

	int root = 0, receiver = 1;
  clock_t start, end;
	//MPI_Status status;

	if (MPI_Init(&argc, &argv) != MPI_SUCCESS) {
		printf("Init failed!\n");
		return -1;
	}

  int my_rank, size;
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  if(my_rank == root){
    int bufsize;
    for (bufsize = 1; bufsize < 1024*1024; bufsize *= 2){
      long buf[bufsize];
      //fill_buf(&buf, bufsize);
      start = clock();
      MPI_Send(&buf, 1, MPI_INT, receiver, 0, MPI_COMM_WORLD);
      end = clock();
      printf("Root sent buffer of size %d. Elapsed: %f\n", bufsize, (end-start)/(double)CLOCKS_PER_SEC);
    }
	}

  MPI_Finalize();
	return 0;
}



////////////////////////////////////////////////////////////////////////////
//
//
// Die logarithmische Laufzeit von MPI_Bcast kann mithilfe einer
// Baumstrukturartigen Verteilung von MPI_Send-Befehlen erreicht werden.
//
//                                1
//                              /   \       MPI_SEND
//                             2     3
//                           /   \ /   \    MPI_SEND
//                          4    5 6   ...
//
int aufgabe3(int argc, char** argv){
	int buf = 42;
	int root = 0;
	int inbuf;
	int receiver1, receiver2;
  double start, end;

	MPI_Status status;

	if(MPI_Init(&argc, &argv) != MPI_SUCCESS){
		printf("init failed!\n");
		//bricht ab
		return -1;
	}
	int my_rank, size;
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	if(my_rank == root){
    start = MPI_Wtime();
		receiver1 = 1;
		receiver2 = 2;
		MPI_Send(&buf, 1, MPI_INT, receiver1, 0, MPI_COMM_WORLD);
		MPI_Send(&buf, 1, MPI_INT, receiver2, 0, MPI_COMM_WORLD);
		printf("process %d: sent %d to process %d\n", my_rank, buf, receiver1);
		printf("process %d: sent %d to process %d\n", my_rank, buf, receiver2);

	}
	else {
		int own_root;
		if(my_rank % 2 == 1){
			own_root = (my_rank + 1) / 2 -1; //1, 3, 5 ...
		}
		else {
			own_root = (my_rank) / 2 - 1; // 2, 4, 6
		}
		MPI_Recv(&inbuf, 1, MPI_INT, own_root, 0, MPI_COMM_WORLD, &status);
    printf("process %d: received %d from process %d\n", my_rank, inbuf, own_root);
		receiver1 = (my_rank + 1) * 2 - 1; //
		receiver2 = (my_rank + 1) * 2;

		printf("process %d: receivers: %d, %d\n", my_rank, receiver1, receiver2);
		if(receiver1 < size) {
			MPI_Send(&inbuf, 1, MPI_INT, receiver1, 0, MPI_COMM_WORLD);
			printf("process %d: sent %d to process %d\n", my_rank, inbuf, receiver1);
		}
		if(receiver2 < size) {
			MPI_Send(&inbuf, 1, MPI_INT, receiver2, 0, MPI_COMM_WORLD);
			printf("process %d: sent %d to process %d\n", my_rank, inbuf, receiver2);
		}
	}
  if (my_rank == root) {
    end = MPI_Wtime();
  }
	MPI_Finalize();

  if (my_rank == root) {
      printf("Runtime = %f\n", end-start);
  }
	return 0;
}
