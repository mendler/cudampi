#!/bin/bash

# Creates a bulk file of given size.

if(($# > 0))
then
	size=$1
	bytes=$(($1*1024*1024))
  filename="testfile${size}MB"
	echo "Creating file $filename..."
  dd if=/dev/zero of=$filename bs=$bytes count=1
else
	echo "Usage: $0 <filesize_in_MB>"
fi
