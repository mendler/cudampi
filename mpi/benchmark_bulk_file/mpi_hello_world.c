#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <string.h>
#include <inttypes.h>

/*
 * MPI Benchmark tool
 * Transfer an increasing amount of data (byteblocks) from one root node to
 * n child nodes and measure the necessary time.
 *
 * Compile with:
 * mpicc -lrt mpi_hello_world.c 
 *
 * MPI-run
 * 
 */

int64_t diff(struct timespec *start, struct timespec *end) {
	return ((end->tv_sec * 1000000000) + end->tv_nsec) -
	((start->tv_sec * 1000000000) + start->tv_nsec);
}

#define TAG 1   /* Communication tag */
#define ROOT 0

int rank, size; /* Rank of each processor and number of processors in total */

int main(int argc, char *argv[])
{
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    int len;
    char name[MPI_MAX_PROCESSOR_NAME];
    MPI_Get_processor_name(name, &len);

    printf("Hello world from process %d of %d on %s.\n", rank, size, name);
    struct timespec start, end;
    double t1, t2;

    /* Create large file */
    char *block;
    uint64_t mb;

    for (mb=1; mb <= 1024*1024; mb <<= 1) {
	    uint64_t block_size = mb*1048576LLU;

	    block = (char*) malloc(block_size);

	    if (rank==ROOT) {
		    //clock_gettime(CLOCK_MONOTONIC, &start);
		    t1 = MPI_Wtime();
	    }

	    /* BENCHMARK START */
	    MPI_Barrier(MPI_COMM_WORLD);

	    if (rank==ROOT)
	    {

		    int current_processor;
		    for (current_processor = 1; current_processor < size; ++current_processor) {
			    //printf("Sending %d bytes\n", block_size);
			    //MPI_Send(block, block_size, MPI_CHAR, 1, TAG, MPI_COMM_WORLD);
			    MPI_Send(block, block_size, MPI_CHAR, current_processor, TAG, MPI_COMM_WORLD);
		    }
	    } else { /* CHILDREN */
		    MPI_Status status;
		    MPI_Recv(block, block_size, MPI_CHAR, ROOT, TAG, MPI_COMM_WORLD, &status);
	    }

	    MPI_Barrier(MPI_COMM_WORLD);
	    /* BENCHMARK STOP */

	    if (rank==ROOT) {
		    //clock_gettime(CLOCK_MONOTONIC, &end);
		    //int64_t timeElapsed = diff(&start, &end);
		    //printf("Transfering %d MB. Time difference: %ld ns (%ld ms)\n", mb, timeElapsed, timeElapsed/1000000);
		    t2 = MPI_Wtime();
		    printf("%d %lf\n", mb,t2-t1);
	    }
    }

    MPI_Finalize();
    return 0;
}
