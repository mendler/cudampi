#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <string.h>

#define TAG 1   /* Communication tag */
#define DEBUG 1
int rank, size; /* Rank of each processor and number of processors in total */

int get_file_size(FILE* file)
{
  fseek(file, 0L, SEEK_END);
  int sz = ftell(file);
  fseek(file, 0L, SEEK_SET);
  return sz;
}

char* read_from_file(FILE* file)
{
  int i = 0;

  #ifdef DEBUG
    printf("Reading %d bytes from file...\n", sz);
  #endif

  char* data = (char*) malloc(sz);

  fscanf(file, "%c", &data[i]);
  while (!feof(file))
  {
      i++;
      //printf ("%d ", i);
      fscanf(file, "%c", &data[i]);
  }
  return data;
}

int* read_ints (const char* file_name)
{
  FILE* file = fopen(file_name, "r");
  int i = 0;

  fseek(file, 0L, SEEK_END);
  int sz = ftell(file);
  fseek(file, 0L, SEEK_SET);
  #ifdef DEBUG
    printf("Reading %d bytes from file...\n", sz);
  #endif

  int* ints = (int*) malloc(sz);

  fscanf(file, "%d", &ints[i]);
  while (!feof (file))
  {
      i++;
      //printf ("%d ", i);
      fscanf (file, "%d", &ints[i]);
  }
  fclose (file);
  return ints;
}

void print_array(int* arr, size_t len) {
    int i;
    for (i=0; i<len; ++i)
      printf("%d ", arr[i]);
}

int main(int argc, char *argv[])
{
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if (argc < 2) {
      MPI_Finalize();
      return -1;
    }

    if (rank==0)
    {
      /* Read file */
      printf("Loading %s...", argv[1]);
      //int *ints = read_ints(argv[1]);
      FILE* file = fopen(argv[1], "r");
      int sz = get_file_size(file);
      char *data = read_from_file(file, sz);
      fclose (file);
      printf("done.\n");

      /* Start benchmark */
      int send_size = sz / size;
      printf("Sending %d bytes to each processor.\n", send_size);

      /*
      int len;
      char name[MPI_MAX_PROCESSOR_NAME];
      MPI_Get_processor_name(name, &len);
      printf("Hello world from process %d of %d on %s.\n", rank, size, name);
      MPI_Barrier(MPI_COMM_WORLD);
      */

    } else {

    }
    /*
    const int recvsize = 50;
    int *sendbuf, recvbuf[recvsize];
    int sendsize = nb_proc*recvsize;
    sendbuf = new int[sendsize];
    if (proc_id == 0)
      Generate_data(sendbuf, sendsize);
    MPI_Scatter(sendbuf, recvsize, MPI_INT, recvbuf, recvsize, MPI_INT, 0, MPI_COMM_WORLD);
    for (i=0; i<nb_proc; i++)
      Print_data(recvbuf, recvsize);
    */
    MPI_Finalize();
    return 0;
}
