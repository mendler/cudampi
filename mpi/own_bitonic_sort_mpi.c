#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <string.h>
#include "quicksort.c"

#define TAG 1 /* Communication tag */

/* Run this program knowing that:
 * 1) The number of cores must be a power of 2
 * 2) The length of the array to order must be a power of 2
 *
 * Exec Example: mpirun -n 4 ./bs 1024 2056
 * Where
 *  number of processors = 4
 *  number of elements = 1024
 *  max value = 2056
 */

void exchange(FILE *log, int i, int partner, int up);

int *myArray, *partnerArray;  /* Arrays to exchange values with. */
int currentPartner = -1;      /* Exchange partner */
int rank, size;               /* Rank of each processor and number of processors in total */
MPI_Status status;
int verbose = 1;              /* this var toggles on(1) or off(0) some useful prints for debugging purpose */
int amount=0;                 /* Chunk size for each processor */

void printArray(FILE *log, int id, int* array, int size) {
  int i;
  printf("Processor %d: ", id);
  for (i = 0; i < size; ++i) {
      printf ("%d ", array[i]);
  }
      printf ("\n");
}

int main(int argc, char *argv[])
{
    int *array;   /* Field of numbers for each local processor */
    int i=0;
    int carry=0;  /* Padding so that each local field has the same length */
    int up=1;     /* Sort ascending (up=1) or descending (up=0) */
    int count=0;  /* Amount of elements to sort in total */

    struct timeval tim; /* Measure time */

    /* Logfile for testing purposes */
    FILE *log;
    char logName[15] = "log/";

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    /* Time meter */
    srand((double) time(NULL));
    gettimeofday(&tim, NULL);
    double t1=tim.tv_sec+(tim.tv_usec/1000000.0);

    log = fopen(logName,"w");
    //snprintf(logName+4, 10, "%d",rank);

    printf("Hello world from process %d of %d.\n", rank, size);
    MPI_Barrier(MPI_COMM_WORLD);

    /* INPUT */

    if (rank==0)
    {
        if (argc==2) /* by file */
        {
            FILE *input = fopen(argv[1],"r");
            char line[20];
            count = 0;
            while(fgets(line,20,input) != NULL)
            {
                count++;
            }
            fclose(input);
            array = (int *)malloc(count*sizeof(int));
            input = fopen(argv[1],"r");
            i = 0;
            while(fgets(line,20,input) != NULL)
            {
                array[i] = atoi(line);
                i++;
            }
            fclose(input);
        }
        else
            if (argc==3) /* by command line */
            {
                count = atoi(argv[1]);
                int max = atoi(argv[2]);
                array = (int *)malloc(count*sizeof(int));
                srand(time(NULL));
                for (i=0; i<count; i++)
                {
                    array[i] = rand()%max;
                }
            }
            else
            {
                printf("\n\n ----------- Input error ----------- \n\n");
                return 1;
            }

        /* END OF INPUT */


        if (verbose){
            printArray(log, rank, array, count);
        }

        MPI_Barrier(MPI_COMM_WORLD);

        carry = count%size;  /* Calculate padding */
        amount = count/size + carry;  /* Number of elements for each processor */
        printf("\nParameters: amount=%d carry=%d\n\n", amount, carry);
        up=1;

        /*
         * The elements for the next processor after root start at array[amount]
         * so we set the startIndex to this initial value.
         */
        int startIndex = amount;


        /* Everyone gets a local array with "amount" elements in it. */
        myArray = (int *)malloc(amount*sizeof(int));
        /* Partner field to exchange of values */
        partnerArray = (int *)malloc(amount*sizeof(int));

        for (i=0; i<amount; i++) {
           myArray[i] = array[i];
        }

        printf("Processor %d received amount=%d and up=%d\n", rank, amount, up);
        if (verbose) {
          printArray(log, rank, myArray, amount);
        }

        /* Root process sending the big array's chunks */
        for (i=1; i<size; i++)
        {
            up = (i+1) % 2;
            MPI_Send(&up, 1, MPI_INT, i, TAG, MPI_COMM_WORLD);
            MPI_Send(&amount, 1, MPI_INT, i, TAG, MPI_COMM_WORLD);
            MPI_Send(&carry, 1, MPI_INT, i, TAG, MPI_COMM_WORLD);

            MPI_Send(array+startIndex, amount-carry, MPI_INT, i, TAG, MPI_COMM_WORLD);

            startIndex += amount-carry;
        }

        MPI_Barrier(MPI_COMM_WORLD);
    }
    else // Child processes
    {
        MPI_Barrier(MPI_COMM_WORLD);

        MPI_Recv(&up, 1, MPI_INT, 0, TAG, MPI_COMM_WORLD, &status);
        MPI_Recv(&amount, 1, MPI_INT, 0, TAG, MPI_COMM_WORLD, &status);
        MPI_Recv(&carry, 1, MPI_INT, 0, TAG, MPI_COMM_WORLD, &status);
        myArray = (int *)malloc(amount*sizeof(int));
        partnerArray = (int *)malloc(amount*sizeof(int)); /* Buffer (partner) */
        MPI_Recv(myArray, amount, MPI_INT, 0, TAG, MPI_COMM_WORLD, &status);

        /* Experimental padding: every chunk has the same amount of items. */
        for (i=amount-carry; i<amount; i++)
        {
            myArray[i] = 0;
        }

        printf("\n");
        printf("Processor %d received amount=%d and up=%d\n", rank, amount-carry, up);

        if (verbose){
          printArray(log, rank, myArray, amount);
        }
        MPI_Barrier(MPI_COMM_WORLD);
    }

    /* CORE */


    /* Do an initial local quicksort before exchanging values with other processors */
    //quickSort(&myArray[0], 0, amount);
    if (verbose) {
      printArray(log, rank, myArray, amount);
    }

    int j;

  /* Major step */
  int k;
  int NUM_VALS = amount*size;

  for (k = 2; k <= NUM_VALS; k <<= 1) {
    printf("k=%d\n", k);
    /* Minor step */
    for (j=k>>1; j>0; j=j>>1) {
        //bitonic_sort_step<<<blocks, threads>>>(dev_values, j, k);
        //int start = rank*amount;
        //int end = (rank+1)*amount;
        //for (i=start;i<end;i++) {
        for (i=0;i<NUM_VALS;i++) {
          int partner=i^j;
          if ((partner)>i) {
              printf("Exchange:\ti=%d\tpartner=%d\ti&k=%d\n", i, partner, i&k);
              exchange(log,i,partner,i&k);
          }
        }
    }
  }


    /*
    for (up=8;up<=amount*size;up=2*up)
    {
        for (j=up>>1;j>0;j=j>>1)
        {
             // Amount = Numbers per processor
             // size = WORLD_SIZE (number of processors)
            for (i=0;i<amount*size;i++)
            {
                int partner=i^j;
                if ((partner)>i) {
                    //printf("Exchange: i=%d, partner=%d, i&up=%d\n", i, partner, i&up);
                    exchange(log,i,partner,i&up);
                }

            }
        }
    }
    */

    /* END OF THE CORE */

    /* Gather results in root process */
    if (rank==0) {
        myArray = (int *)realloc(myArray,sizeof(int)*amount*size);
    }
    MPI_Gather(myArray, amount, MPI_INT, array, amount, MPI_INT, 0, MPI_COMM_WORLD);
    if (rank==0) {
        printArray(log, rank, array,count);
    }

    /*
    if (rank!=0)
    {
        MPI_Send(myArray, amount, MPI_INT, 0, TAG, MPI_COMM_WORLD);
    }
    gettimeofday(&tim, NULL);
    double t2=tim.tv_sec+(tim.tv_usec/1000000.0);
    if (rank==0)
    {
        myArray = (int *)realloc(myArray,sizeof(int)*amount*size);
        for (i=1; i<size; i++)
            MPI_Recv(myArray+i*amount, amount, MPI_INT, i, TAG, MPI_COMM_WORLD, &status);
        printf("\Excecution speed: %6f\n", t2-t1);
        //printArray(log,array,count);
        //printArray(log,myArray+(carry*(size-1)),count);
        //printArray(log,myArray,amount*size);

    }
    */
    fclose(log);
    MPI_Finalize();
    return 0;
}

void exchange(FILE *log, int i, int partner, int up)
{
    int rank_i = i/amount;              /* Processor which posesses the first value */
    int rank_partner = partner/amount;  /* Processor which posesses the second value */

    /* Offsets from beginning of local fields */
    int offset_i = i % amount;
    int offset_partner = partner % amount;

    if ((rank_i != rank) && (rank_partner != rank)) {
        /*
         * The current processor is not part of the exchange,
         * since it doesn't have the required value in its local field.
         */
        return;
    }

    //printArray(log, rank, myArray, amount);

    /* -- Local exchange -- */
    if ((rank_i == rank) && (rank_partner == rank))
    {
        /*
         * If we sort descending and the first value is bigger than the second or
         * If we sort ascending and the first value is smaller than the second
         * we need to swap the values.
         */
        if (((up==0) && (myArray[offset_i] > myArray[offset_partner])) || ((up!=0) && (myArray[offset_i] < myArray[offset_partner])))
        {
            /* Swap values */
            int temp = myArray[offset_i];
            myArray[offset_i] = myArray[offset_partner];
            myArray[offset_partner] = temp;
        }
        //return;
    }

    /* -- Global exchange - Own rank -- */
    if (rank_i == rank && rank_partner != rank)
    {
        if (currentPartner != rank_partner)
        {
            /* We must transfer the local arrays before sorting */
            MPI_Send(myArray, amount, MPI_INT, rank_partner, TAG, MPI_COMM_WORLD);
            MPI_Recv(partnerArray, amount, MPI_INT, rank_partner, TAG, MPI_COMM_WORLD, &status);
            currentPartner = rank_partner;
        }
        if (((up==0) && (myArray[offset_i] > partnerArray[offset_partner])) || ((up!=0) && (myArray[offset_i] < partnerArray[offset_partner])))
            myArray[offset_i] = partnerArray[offset_partner];
        //return;
    }

    /* -- Global exchange - Partner -- */
    if (rank_i != rank && rank_partner == rank)
    {
        if (currentPartner != rank_i)
        {
            /* We must transfer the local arrays before sorting */
            MPI_Recv(partnerArray, amount, MPI_INT, rank_i, TAG, MPI_COMM_WORLD, &status);
            MPI_Send(myArray, amount, MPI_INT, rank_i, TAG, MPI_COMM_WORLD);
            currentPartner = rank_i;
        }
        if (((up==0) && (partnerArray[offset_i] > myArray[offset_partner])) || ((up!=0) && (partnerArray[offset_i] < myArray[offset_partner])))
            myArray[offset_partner] = partnerArray[offset_i];
        //return;
    }
    //printArray(log, rank, myArray, amount);
}
