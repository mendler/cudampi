#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

char *padded_str(int i, int max_val) {
  // Determine how much padding is needed
  int max_len = floor(log10(abs(max_val))) + 1;
  int len = floor(log10(abs(i))) + 1;
  int diff = max_len - len;

  // Add padding to string
  int count;
  char *padded_str = (char*) malloc((max_len + 1)*sizeof(char));
  for (count = 0; count < diff; ++count) {
      padded_str[count] = '0';
  }

  // Convert number i to string
  char str_i[len];
  sprintf(str_i,"%d",i);

  // Add the number i to string
  strcat(padded_str, str_i);
  return padded_str;
}

main() {
  int num_buckets = 1000000;
  int size = 8;
  int i = 10;
  int rank = 2;

  char bucket_file[num_buckets + size + 2];
  char str_rank[size];

  // The filename consists of the following elements:
  strcat(bucket_file, padded_str(i, num_buckets));

  strcat(bucket_file, "_");   // Separator

  sprintf(str_rank,"%d",rank);
  strcat(bucket_file, str_rank);  // Processor rank

  printf("Closing file %s\n", bucket_file);
}
