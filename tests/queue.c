#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <time.h>
#include <math.h>
#include <string.h>

#include <pthread.h>

#define NUM_BUCKETS 20

void sort_queue() {
  int current_bucket = 0;
  int current_process = 0;

  int size, i;
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  for (i = 0; i < size; ++i, ++current_bucket, ++current_process) {
    MPI_Send(&current_bucket, 1, MPI_INT, current_process, 0, MPI_COMM_WORLD);
  }

  while (current_bucket < NUM_BUCKETS) {
    int answer;
    MPI_Status status;
    MPI_Recv(&answer, 1, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);

    current_process = status.MPI_SOURCE;
    if (++current_bucket > NUM_BUCKETS) {
      current_bucket = -1;
    }
    printf("Sending %d\n", current_bucket);
    MPI_Send(&current_bucket, 1, MPI_INT, current_process, 0, MPI_COMM_WORLD);
  }

  //pthread_exit(0);
}

void sort_next_bucket() {

  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  int current_bucket;
  while (1) {
    MPI_Status status;
    MPI_Recv(&current_bucket, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);
    if (current_bucket == -1) {
      // All work done
      printf("%d: DONE\n", rank);
      break;
    }
    printf("%d: BUCKET %d\n", rank, current_bucket);
    int sort_status = 1;
    MPI_Send(&sort_status, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
  }
}

void sort(int rank) {
  pthread_t queue_thread;
  if (rank == 0) {
    // Start queue thread
    sort_queue();
  } else {
    // Do work on all processes
    sort_next_bucket();
  }
}

int main(int argc, char** argv) {
  int provided;
  int rc = MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);

  if (rc != MPI_SUCCESS) {
    printf("Error starting MPI. Aborting\n");
    MPI_Abort(MPI_COMM_WORLD, rc);
  }

  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  sort(rank);

  MPI_Finalize();
  return 0;
}
