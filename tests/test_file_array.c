#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <string.h>
#define DEBUG

#ifdef DEBUG
    #define DEBUG_PRINT printf
#else
    #define DEBUG_PRINT
#endif

int main(int argc, char *argv[])
{
    int rc = MPI_Init(&argc, &argv);
    if (rc != MPI_SUCCESS) {
        printf("Error starting MPI. Aborting\n");
        MPI_Abort(MPI_COMM_WORLD, rc);
    }


    int rank, size; /* Rank of each processor and number of processors in total */
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    MPI_File *handles = (MPI_File *) malloc( 10*sizeof(MPI_File));

    int i;
    for (i = 0; i < 10; ++i) {
      char filename[256];
      sprintf(filename,"%d",i);
      MPI_File_open(MPI_COMM_WORLD, filename, (MPI_MODE_WRONLY | MPI_MODE_CREATE), MPI_INFO_NULL, &handles[i]);
    }

    // Do work
    float buf[100];
    for (i = 0; i < 100; ++i) {
          int x = random();
          float f = (float) x / RAND_MAX;
          buf[i] = f;
    }

    MPI_Status status;
    int interval = 1;
    int num_buckets = 10;
    for (i = 0; i < 100; ++i) {
      int bucket = floor( (buf[i]/interval) * num_buckets);
      MPI_File_write(handles[bucket], &buf[i], 1, MPI_FLOAT, &status);
    }

    for (i = 0; i < 10; ++i) {
      MPI_File_close(&handles[i]);
    }


    MPI_Finalize();
    return 0;
}
