#include <cuda.h> // to get memory on the device  
#include <cuda_runtime.h> // to get device count  
#include <stdlib.h>
#include <stdio.h>

int main( int argc, char** argv)   
{  
    //get device count  
    int deviceCount = 0;  
    cudaError_t error_id = cudaGetDeviceCount(&deviceCount);  
    if(deviceCount == 0)  
    {  
//        cout << "Error no cuda devices";  
    printf("Error no cuda devices");
        return 1;  
    }  
  
    //get the first cuda device  
    CUdevice cudaDevice;  
    CUresult result = cuDeviceGet(&cudaDevice, 0);  
    if(result!= CUDA_SUCCESS)  
    {  
 //       cout << "Error fetching cuda device";  
    printf("Error cuda device");
        return 1;         
    }  
  
    //create cuda context  
    CUcontext cudaContext;    
    result = cuCtxCreate(&cudaContext, CU_CTX_SCHED_AUTO, cudaDevice);  
    if(result != CUDA_SUCCESS)  
    {  
  //      cout << "Error creating cuda context";  
    printf("Error cuda context");
        return 1;         
    }  
  
    //get the amount of free memory on the graphics card  
    size_t available, total;
    cudaMemGetInfo(&available, &total);
    printf("Available: %ld, Total: %ld\n", available, total);

    return 0;  
}
