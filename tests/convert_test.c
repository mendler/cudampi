#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <string.h>
#define DEBUG

#ifdef DEBUG
    #define DEBUG_PRINT printf
#else
    #define DEBUG_PRINT
#endif

void close_buckets(num_buckets, rank, size) {
    int i;
    for (i = 0; i < num_buckets; ++i) {

      int len_buckets = floor(log10(abs(num_buckets))) + 1;
      int len_size = floor(log10(abs(size))) + 1;

      char bucket_file[len_buckets + len_size + 1];

      sprintf(bucket_file,"%0*d",len_buckets, i);
      strcat(bucket_file, "_");   // Separator

      char str_rank[len_size];
      sprintf(str_rank,"%d",rank);
      strcat(bucket_file, str_rank);  // Processor rank

      DEBUG_PRINT("Closing file %s\n", bucket_file);
    }
    //MPI_File_close(&data);
}

main() {
  close_buckets(101, 200, 200);
}


