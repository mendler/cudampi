#include <dirent.h>
#include <stdio.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define BUCKET_DIR "."
#define MAXPATHLEN 255

int main(int argc,char* argv[]){
  DIR* myDirectory;
  int result;
  char* fullpath;
  char path[MAXPATHLEN];

  if (getcwd(path, sizeof(path)) == NULL) {
    printf("getcwd() error");
    exit(-1);
  } else {
    printf("current working directory is: %s\n", path);
  }

  strcat(path,"/");
  myDirectory=opendir(path);

  struct stat fileProperties;
  struct dirent* directory;

  do {
    directory=readdir(myDirectory);
    if (directory!=NULL) {

      fullpath= (char*) malloc((strlen(path)+strlen(directory->d_name)+2 ) * sizeof(char));
      strcat(fullpath,path);
      strcat(fullpath,directory->d_name);

      result=lstat(fullpath,&fileProperties);

      switch (fileProperties.st_mode & S_IFMT) {
        case S_IFDIR:
              printf("DIR ");
              break;
        case S_IFLNK:
              printf("LINK ");
              break;
        case S_IFREG:
              printf("FILE ");
              break;
        default:
              printf("OTHER ");
              break;
        }
        printf("%s\n", directory->d_name);
    }
  } while(directory!=NULL);

  closedir(myDirectory);
  free(fullpath);

  return 0;
}
