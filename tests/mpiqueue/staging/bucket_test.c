#include <dirent.h>
#include <stdio.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define BUCKET_DIR "buckets"
#define WORKING_DIR "working"
#define MAXPATHLEN 255
#define FILENAME_SEPARATOR '_'

char* next_bucket_filename() {
  DIR* myDirectory;
  int result;
  char* fullpath;
  char path[MAXPATHLEN];

  if (getcwd(path, sizeof(path)) == NULL) {
    //printf("getcwd() error");
    exit(-1);
  } else {
    //printf("current working directory is: %s\n", path);
  }

  strcat(path,"/");
  strcat(path,BUCKET_DIR);
  strcat(path,"/");
  myDirectory=opendir(path);

  struct stat fileProperties;
  struct dirent* directory;

  do {
    directory=readdir(myDirectory);
    if (directory != NULL) {

      fullpath = (char*) malloc((strlen(path)+strlen(directory->d_name)+2 ) * sizeof(char));
      strcat(fullpath,path);
      strcat(fullpath,directory->d_name);

      result=lstat(fullpath,&fileProperties);

      if ((fileProperties.st_mode & S_IFMT) == S_IFREG) {
        // File found. Get bucket id from filename

        char *found;
        // Find filename separator
        // (filename: bucketid + FILENAME_SEPARATOR + pid)
        found = strchr(directory->d_name, FILENAME_SEPARATOR);

        if (found) {
            // Get length of bucket id substring in filename
            size_t len_bucket_id = found - directory->d_name;

            char filename_pid[2];
            filename_pid[1] = directory->d_name[len_bucket_id + 1];
            int pid = atoi(filename_pid);

            if (pid == 0) {
              //printf("File with pid=0 found. Filename: %s\n", directory->d_name);
              char *bucket = malloc(len_bucket_id + 1);
              memcpy(bucket, directory->d_name, len_bucket_id);
              bucket[len_bucket_id] = '\0';

              closedir(myDirectory);
              free(fullpath);
              return bucket;
            }
        }
      }
    }
  } while(directory!=NULL);
  return NULL; // No remaining buckets found
}

void sort() {
  char *bucket_id;
  bucket_id = next_bucket_filename();
  if (bucket_id) {
    load_buckets(bucket_id);
  }
}

int load_buckets(char *bucket_name) {
  size_t bucket_dir_len = strlen(BUCKET_DIR);
  size_t working_dir_len = strlen(WORKING_DIR);

  // Allocate space for bucket filename
  // Extra padding for:
  //  - Path separator
  //  - Filename separator
  //  - Terminating \0 character
  int len_old_bucket_path = bucket_dir_len +  strlen(bucket_name) + 10 + 3; // FIXME: use processor size instead of 10
  int len_new_bucket_path = working_dir_len + strlen(bucket_name) + 10 + 3;

  char *old_bucket_path = (char*) malloc(len_old_bucket_path);
  char *new_bucket_path = (char*) malloc(len_new_bucket_path);

  strcpy(old_bucket_path, BUCKET_DIR);
  strcat(old_bucket_path, "/");
  strcat(old_bucket_path, bucket_name);
  old_bucket_path[bucket_dir_len + strlen(bucket_name) + 1] = FILENAME_SEPARATOR;
  old_bucket_path[bucket_dir_len + strlen(bucket_name) + 2] = '\0';

  strcpy(new_bucket_path, WORKING_DIR);
  strcat(new_bucket_path, "/");
  strcat(new_bucket_path, bucket_name);
  new_bucket_path[working_dir_len + strlen(bucket_name) + 1] = FILENAME_SEPARATOR;
  new_bucket_path[bucket_dir_len + strlen(bucket_name) + 2] = '\0';

  printf("%s\n", old_bucket_path);
  printf("%s\n", new_bucket_path);

  //rename(old_bucket_path, new_bucket_path);

  free(old_bucket_path);
  free(new_bucket_path);
}

int main(int argc,char* argv[]){
  sort();
  return 0;
}
