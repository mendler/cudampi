/* Test a self-managing queue using MPI-IO */

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include<dirent.h>

#define NUM_BUCKETS 20
#define BUCKET_DIR "."

/*
 * As an intial sorting step, each processor generates a number buckets (which are files) which
 * contain numbers of a given interval.
 * These files have the following naming scheme:
 * b_p where
 *  b is the number of the current bucket (ranging from 0 to num_buckets)
 *  p is the processor number
 *  The bucket numbers are zero padded (e.g. 0020 when there are 1000 buckets in total).
 */
char *get_bucket_filename(curr_bucket, num_buckets, rank, size) {
    int len_buckets = floor(log10(abs(num_buckets))) + 1;
    int len_size = floor(log10(abs(size))) + 1;

    char *bucket_file = (char*) malloc((len_buckets + len_size + 1)*sizeof(char));

    // The first part of the filename is the bucket number which gets
    // padded with zeros.
    sprintf(bucket_file, "%0*d", len_buckets, curr_bucket);
    strcat(bucket_file, "_");   // Separator

    char str_rank[len_size];
    sprintf(str_rank,"%d",rank);
    strcat(bucket_file, str_rank);  // Processor rank
    return bucket_file;
}

void sort_next_bucket() {

  int rank, size;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  int current_bucket;
  while (1) {
    MPI_Status status;
    MPI_Recv(&current_bucket, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);
    if (current_bucket == -1) {
      // All work done
      printf("%d: DONE\n", rank);
      break;
    }
    printf("%d: BUCKET %d\n", rank, current_bucket);
    int sort_status = 1;
    MPI_Send(&sort_status, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
  }
}

/*
 * See: http://stackoverflow.com/questions/6383584/check-if-a-directory-is-empty-using-c-on-linux
 */
int directory_empty(char *dirname) {
  int n = 0;
  struct dirent *d;
  DIR *dir = opendir(dirname);
  if (dir == NULL) //Not a directory or doesn't exist
    return 1;
  while ((d = readdir(dir)) != NULL) {
    if(++n > 2)
      break;
  }
  closedir(dir);
  if (n <= 2) //Directory Empty
    return 1;
  else
    return 0;
}

int has_buckets() {
  // Simple check bucket directory is empty
  return directory_empty(BUCKET_DIR);
}

void sort() {
  while (has_buckets()) {
    sort_next_bucket();
  }
}

int main(int argc, char** argv) {
  int provided;

  int rc = MPI_Init(&argc, &argv);
  if (rc != MPI_SUCCESS) {
      printf("Error starting MPI. Aborting\n");
      MPI_Abort(MPI_COMM_WORLD, rc);
  }

  int rank, size;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  sort();

  MPI_Finalize();
  return 0;
}
