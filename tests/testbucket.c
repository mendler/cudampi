#include <math.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define MAX_NUM 1
#define MIN_NUM 0
#define MEGABYTE (1024*1024)
#define NUM_VALS (16*1024*1024*1024UL)
#define MAX_BUCKET_SIZE (16*1024*1024UL)
#define DEBUG

#ifdef DEBUG
    #define DEBUG_PRINT printf
#else
    #define DEBUG_PRINT
#endif

main() {
    srand(time(NULL));

    int num_buckets = NUM_VALS / MAX_BUCKET_SIZE;
    double interval = MAX_NUM - MIN_NUM;
    DEBUG_PRINT("Number of buckets: %d\n", num_buckets);
    DEBUG_PRINT("Number interval: %d to %d\n", MIN_NUM, MAX_NUM);

    int i;
    for (i = 0; i < 100; ++i) {
      int x = random();
      float f = (float) x / RAND_MAX;
      int bucket = floor( (f/interval) * num_buckets);
      DEBUG_PRINT("%f ==> %d\n", f, bucket);
    }
}
