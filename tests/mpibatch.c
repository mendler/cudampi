/*
 * Send a bucket of sorting data to the GPU via MPI.
 */

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <string.h>
/*
 * Read floating point numbers from file.
 * filename   Name of file on filesystem
 * start      Starting postion to read (in bytes)
 * nfloats    Number of floats to read from file
 */
float *read_floats(char *filename, int start, int nfloats) {
    MPI_File fh;
    MPI_Status status;
    float *buf = (float *) malloc(nfloats * sizeof(float));

    MPI_File_open(MPI_COMM_WORLD, filename, MPI_MODE_RDONLY, MPI_INFO_NULL, &fh);
    MPI_File_seek(fh, start, MPI_SEEK_SET);
    MPI_File_read(fh, buf, nfloats, MPI_FLOAT, &status);
    MPI_File_close(&fh);
    return buf;
}

int main(int argc, char *argv[])
{
    // Config
    char filename[] = "data.dat";
    int numfloats = 1024*1024;

    MPI_Init(&argc, &argv);

    //int rank, size; /* Rank of each processor and number of processors in total */
    //MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    //MPI_Comm_size(MPI_COMM_WORLD, &size);

    //printf("Hello world from process %d of %d on %s.\n", rank, size, name);
    //MPI_Barrier(MPI_COMM_WORLD);

    float * buf = read_floats(filename, 0, numfloats);

    int i;
    for (i = numfloats - 10; i < numfloats; ++i) {
        printf("%f ", buf[i]);
    }

    free(buf);
    MPI_Finalize();
    return 0;
}
