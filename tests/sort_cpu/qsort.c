/* qsort example */
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

int ELEMENTS;

/**
 * Generate a pseudo random float value
 */
float random_float()
{
  float r = (float)rand()/(float)RAND_MAX;
  return r;
}

/**
 * Print an array
 */
void array_print(float *arr, size_t size)
{
  size_t i;
  for (i = 0; i < size; ++i) {
    printf("%1.3f ", arr[i]);
  }
  printf("\n");
}

/**
 * Fill an array with random floating-point values
 */
void array_fill(float *arr, size_t size)
{
  srand(time(NULL));
  size_t i;
  for (i = 0; i < size; ++i) {
    arr[i] = random_float();
  }
}

double get_elapsed(clock_t start, clock_t stop)
{
  double elapsed = ((double) (stop - start)) / CLOCKS_PER_SEC;
  return elapsed;
}

int read_elements() {
  printf("Number of elements (must be a power of 2)? ");
  int elem;
  scanf("%d", &elem);
  return elem;
}

int floatcomp(const void* elem1, const void* elem2)
{
    if(*(const float*)elem1 < *(const float*)elem2)
        return -1;
    return *(const float*)elem1 > *(const float*)elem2;
}


int main(void)
{

  ELEMENTS = read_elements();

  /* Generate a large number of floating-point values */
  float *arr = (float *) malloc(ELEMENTS * sizeof(float));
  array_fill(arr, ELEMENTS);
  //array_print(arr, ELEMENTS);

  clock_t start, stop;

  start = clock();
  qsort (arr, ELEMENTS, sizeof(float), floatcomp);
  stop = clock();

  double elapsed = get_elapsed(start, stop);
  printf("Elements: %d\t\tElapsed: %.3fs\n", ELEMENTS, elapsed);
  //array_print(arr, ELEMENTS);
}
