/*
 * Bitonic Sort
 * ============
 *
 * Simple CPU implementation.
 *
 * Derived from:
 * http://www.tools-of-computing.com/tc/CS/Sorts/bitonic_sort.htm
 * http://forums.nvidia.com/index.php?showtopic=84651
 */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

int ELEMENTS;

// Radix sort that treats floats as ints and sorts them
void radix(float *array, int count) {
  int i,j;
  for (i=0; i < count; ++i){
    unsigned int radix=(1 << i);

    float *zwarray = (float*) malloc (count * sizeof(float));

    // count how many 0-value-radixes we have
    int count0=0;
    int count1=0;

    for (j=0; j < count; ++j){
      if (!((*(int*)&(array[j]))&radix)){
        ++count1;
      }
    }

    for (j=0; j < count; ++j){
      if ((*(int*)&(array[j]))&radix){
        zwarray[count1]=array[j];
        ++count1;
      }
      else {
        zwarray[count0]=array[j];
        ++count0;
      }
    }

    for (j=0; j < count; ++j){
      array[j]=zwarray[j];
    }

    free(zwarray);
  }
}

/**
 * Generate a pseudo random float value
 */
float random_float()
{
  float r = (float)rand()/(float)RAND_MAX;
  return r;
}

/**
 * Print an array
 */
void array_print(float *arr, size_t size)
{
  size_t i;
  for (i = 0; i < size; ++i) {
    printf("%1.3f ", arr[i]);
  }
  printf("\n");
}

/**
 * Fill an array with random floating-point values
 */
void array_fill(float *arr, size_t size)
{
  srand(time(NULL));
  size_t i;
  for (i = 0; i < size; ++i) {
    arr[i] = random_float();
  }
}

double get_elapsed(clock_t start, clock_t stop)
{
  double elapsed = ((double) (stop - start)) / CLOCKS_PER_SEC;
  return elapsed;
}

int read_elements() {
  printf("Number of elements (must be a power of 2)? ");
  int elem;
  scanf("%d", &elem);
  return elem;
}

int main(void)
{

  ELEMENTS = read_elements();

  /* Generate a large number of floating-point values */
  float *arr = (float *) malloc(ELEMENTS * sizeof(float));
  array_fill(arr, ELEMENTS);
  //array_print(arr, ELEMENTS);

  clock_t start, stop;

  start = clock();
  //bitonic_sort(arr);
  radix(arr, ELEMENTS);
  stop = clock();

  double elapsed = get_elapsed(start, stop);
  printf("Elements: %d\t\tElapsed: %.3fs\n", ELEMENTS, elapsed);
  /*array_print(arr, ELEMENTS);*/
}
