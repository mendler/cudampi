#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>

#define BUCKET_BUF_SIZE 4*1024*1024 // Write data to disk using a buffer of this size
#define NUM_BUCKETS 3

// Write buffer. Numbers will be written to disk
// in chunks of this size
typedef struct bucket_buffer_t {
    float buf[BUCKET_BUF_SIZE];
    int num_elements;
} bucket_buffer;

main() {
  bucket_buffer *buffers = (bucket_buffer *) malloc (NUM_BUCKETS * sizeof(bucket_buffer));
  int i;
  for (i = 0; i < NUM_BUCKETS; ++i) {
      printf ("%d: %d\n", i, buffers[0].num_elements);
  }
}
